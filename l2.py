# Copyright 2012 James McCauley
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
A super simple OpenFlow switch that installs rules based on MAC and time of day.
Modified l2_pairs module.
Authors: Zeke Petersen and Devin Perez
"""

# These next two imports are common POX convention
from pox.core import core
import pox.openflow.libopenflow_01 as of
from pox.lib.revent import *
import time
import datetime

# Even a simple usage of the logger is much nicer than print!
log = core.getLogger()

# This table maps port to MAC-addr pairs
# Given our topology, we know these connections already
mac_to_port = {"00:00:00:00:00:01": 2,
               "00:00:00:00:00:02": 4,
	       "00:00:00:00:00:03": 1,
	       "00:00:00:00:00:04": 3}

"""
Hardcoded AC table to return whether or not a user is allowed to access a file.
"""

# Map MAC pairs to valid hours of the day (24 hour clock)
ac_table = {
            ("00:00:00:00:00:01", "00:00:00:00:00:02"): (6, 22), #A
            ("00:00:00:00:00:01", "00:00:00:00:00:03"): (0, 0),  #B
            ("00:00:00:00:00:01", "00:00:00:00:00:04"): (4, 8), #C
            ("00:00:00:00:00:02", "00:00:00:00:00:01"): (6, 22), #A
            ("00:00:00:00:00:02", "00:00:00:00:00:03"): (24, 24), #D
            ("00:00:00:00:00:02", "00:00:00:00:00:04"): (0, 0), #E
            ("00:00:00:00:00:03", "00:00:00:00:00:01"): (0, 0), #B
            ("00:00:00:00:00:03", "00:00:00:00:00:02"): (24, 24), #D
            ("00:00:00:00:00:03", "00:00:00:00:00:04"): (24, 24), #F
            ("00:00:00:00:00:04", "00:00:00:00:00:01"): (4, 8), #C
            ("00:00:00:00:00:04", "00:00:00:00:00:02"): (0, 0), #E
            ("00:00:00:00:00:04", "00:00:00:00:00:03"): (24, 24)} #F

# Arguments are the hours of valid access. True if access granted.
# 24, 24 means always allowed and 0, 0 means never allowed.
def getAccess(start, end):
  # Always have access
  if (start == 0 and end == 0):
    return False
  if (start == 24 and end == 24):
    return True
  now = datetime.datetime.now()
  #log.info(now)
  today_start = now.replace(hour=start, minute=0, second=0, microsecond=0)
  today_end = now.replace(hour=end, minute=0, second=0, microsecond=0)
  return today_start < now < today_end

# Handle messages the switch has sent us because it has no
# matching rule.
def _handle_PacketIn (event):
  packet = event.parsed

  dst_port = mac_to_port[str(packet.dst)]
  #log.info(dst_port)
  msg = of.ofp_flow_mod()
  msg.data = event.ofp 
  msg.match.dl_src = packet.src
  msg.match.dl_dst = packet.dst
  msg.hard_timeout = 3
  start, end = ac_table[(str(packet.src), str(packet.dst))]
  if (getAccess(start, end)):
    msg.actions.append(of.ofp_action_output(port = dst_port))
  else:
    log.info("past your bedtime..")
  event.connection.send(msg)

def launch (disable_flood = False):

  core.openflow.addListenerByName("PacketIn", _handle_PacketIn)
  log.info("Pair-Learning switch running.")
