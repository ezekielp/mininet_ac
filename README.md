# README #

Learning switches using POX are looked for in the pox or ext directories.
These are run with ~/pox/pox.py <modulename>
Example (if l2.py is in the ext directory):

~/pox/pox.py l2

Then one can run the auto script and the connection will be made between the 
mininet network looking for a remote controller and the pox module we just
launched:

sudo python ./auto.py

This will generate a single switch topology with 4 hosts.

Since the learning module cannot be directly run from the repo folder, one needs to move the module into the ext folder before testing or running.

### Description ###

This repo contains code to enforce an access control policy on an emulated
Mininet network using an OpenFlow controller. It installs flow table entries on the switch based on MAC address and time of day.

We used some source code from the Mininet and POX documentation and modified them to create our custom testing framework

### Who do I talk to? ###

Authors:
Devin Perez (ddp@uoregon.edu)
Zeke Petersen (ezekielp@uoregon.edu)
